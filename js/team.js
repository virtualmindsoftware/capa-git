$(function(){
	loadTeam();
});

var teamMembers = 30;
var teamMembersLoaded = 0;

function loadTeam(){
	$('#team-loading').removeClass('hidden');
	$('#team-container').addClass('hidden');
	$('#team-container').find('.row').empty();
	teamMembersLoaded = 0;
	for (var i = 1; i <= teamMembers; i++) {
		loadTeamMember(i);
	}
}

function loadTeamMember(teamMemberIndex)
{
	$.get({
		url: 'team-members/' + teamMemberIndex + '/team-member.json?papa=fasd',
		data: {},
		success: function(data){
			var html = $('<div class="col-lg-4 col-sm-6 text-center mb-4"><img style="width:300px;height:300px;" class="rounded-circle img-fluid d-block mx-auto" src="/team-members/' + teamMemberIndex + '/' + data.image + '" alt=""><h3>' + data.name + '</h3><p>' + data.title + '</p><p style="font-style: italic">' + (data.description ? data.description : '') + '</p></div>');
			$('#team-container').find('.row').append(html);
		},
		error: function(data){
			console.log('fail!');
		},
		complete: function(){
			teamMemberLoaded();
		},
		dataType: 'json'
	});
}

function teamMemberLoaded()
{
	teamMembersLoaded++;
	if (teamMembersLoaded == teamMembers) {
		$('#team-loading').addClass('hidden');
		$('#team-container').removeClass('hidden');
	}
}